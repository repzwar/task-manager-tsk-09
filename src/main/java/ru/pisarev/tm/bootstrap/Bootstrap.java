package ru.pisarev.tm.bootstrap;

import ru.pisarev.tm.api.ICommandController;
import ru.pisarev.tm.api.ICommandRepository;
import ru.pisarev.tm.api.ICommandService;
import ru.pisarev.tm.constant.ArgumentConst;
import ru.pisarev.tm.constant.TerminalConst;
import ru.pisarev.tm.controller.CommandController;
import ru.pisarev.tm.repository.CommandRepository;
import ru.pisarev.tm.service.CommandService;

import java.util.Scanner;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void start(String... args)
    {
        displayWelcome();
        if (runArgs(args)) commandController.exit();
        process();
    }

    private boolean runArgs(final String[] args) {
        if (args == null || args.length == 0) return false;
        run(args[0]);
        return true;
    }

    private void displayWelcome() {
        System.out.println("** WELCOME TO TASK MANAGER **");
    }

    private void run(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConst.CMD_HELP:
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_ABOUT:
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_INFO:
            case ArgumentConst.ARG_INFO:
                commandController.displayInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.displayCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.displayArguments();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
            default:
                displayError();
        }
    }

    private void process() {
        final Scanner scanner = new Scanner(System.in);
        String command = "";
        while (!TerminalConst.CMD_EXIT.equals(command)) {
            commandController.displayWait();
            command = scanner.nextLine();
            run(command);
        }
    }

    private void displayError() {
        System.out.println("Incorrect command. Use " + TerminalConst.CMD_HELP + " for display list of terminal commands.");
    }

}
